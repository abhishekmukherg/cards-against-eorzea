package com.mukherg.discord.cardsagainsteorzea

import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.greaterThanOrEqualTo
import com.natpryce.hamkrest.isIn
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.fail
import org.junit.jupiter.api.Test

class WeightedSublistsTest {
    @Test
    fun `simple weighted get with one list`() {
        val innerList = listOf(1, 2, 3, 4, 5)
        val weightedList = WeightedSublists(innerList to 5)
        val count = weightedList.groupingBy { it }.eachCount()
        count.forEach { (value, count) ->
            assertThat(value, isIn(innerList))
            assertEquals(5, count)
        }
        // just make sure I can actually get every element
        repeat(5 * 5) { weightedList[it] }
    }

    @Test
    fun `two lists unequal weight`() {
        val innerList1 = listOf(1, 2, 3, 4, 5)
        val innerList2 = listOf(7, 8, 9, 10, 11)

        val weightedList = WeightedSublists(
            innerList1 to 5,
            innerList2 to 2
        )
        val count = weightedList.groupingBy { it }.eachCount()
        count.forEach { (value, count) ->
            when {
                innerList1.contains(value) -> {
                    assertEquals(5, count)
                }
                innerList2.contains(value) -> {
                    assertEquals(2, count)
                }
                else -> {
                    fail("found unexpected value $value with $count instances")
                }
            }
        }
        repeat(5 * 5 + 5 * 2) { weightedList[it] }
    }

    @Test
    fun `two lists equal weight`() {
        val innerList1 = listOf(1, 2, 3, 4, 5)
        val innerList2 = listOf(7, 8, 9, 10, 11)

        val weightedList = WeightedSublists(
            innerList1 to 5,
            innerList2 to 5
        )
        val count = weightedList.groupingBy { it }.eachCount()
        count.forEach { (value, count) ->
            when {
                innerList1.contains(value) -> {
                    assertEquals(5, count)
                }
                innerList2.contains(value) -> {
                    assertEquals(5, count)
                }
                else -> {
                    fail("found unexpected value $value with $count instances")
                }
            }
        }
        repeat(5 * 5 + 5 * 5) { weightedList[it] }
    }

    @Test
    fun `index of list`() {
        val innerList1 = listOf(1, 2, 3, 4, 5)
        val innerList2 = listOf(7, 8, 9, 10, 11)

        val weightedList = WeightedSublists(
            innerList1 to 5,
            innerList2 to 2
        )
        (innerList1 + innerList2).forEach {
            val indexOf = weightedList.indexOf(it)
            assertThat("all elements in the sublists should be found by indexOf", indexOf, greaterThanOrEqualTo(0))
            assertEquals(it, weightedList[indexOf]) { "expected for weightedList[$indexOf] == $it" }
        }
    }
}
