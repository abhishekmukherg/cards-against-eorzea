package com.mukherg.discord.cardsagainsteorzea

import java.util.Random
import kotlin.math.abs
import org.junit.jupiter.api.Test

class CardDrawerTest {
    @Test
    fun `randomizer can handle two databases`() {
        val supplier = CardDrawer(listOf("foo", "bar", "baz"), Random(0))
        var countFoo = 0
        var countBar = 0
        var countBaz = 0
        repeat(3000) {
            when (supplier.get()) {
                "foo" -> countFoo += 1
                "bar" -> countBar += 1
                "baz" -> countBaz += 1
            }
        }
        assert(abs(countFoo - countBar) < 50) { "Expected abs($countFoo - $countBar) < 20" }
        assert(abs(countFoo - countBaz) < 50) { "Expected abs($countFoo - $countBaz) < 20" }
    }
}
