package com.mukherg.discord.cardsagainsteorzea

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class GeneratorTest {
    @Test
    fun `with multiple blanks, should return multiple results`() {
        val template = object : ICardDrawer {
            override fun get(): String = "{} {}"
        }

        val whites = object : ICardDrawer {
            val results = listOf("foo", "bar").iterator()
            override fun get() = results.next()
        }

        val generator = Generator()
        Assertions.assertEquals("<foo> <bar>", generator.generate(template, whites))
    }
}
