package com.mukherg.discord.cardsagainsteorzea.commands

import com.mukherg.discord.cardsagainsteorzea.StringDatabaseFactory
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter

class CardsAgainstEorzeaDumpDatabase(
    private val stringDatabaseFactory: StringDatabaseFactory
) : ListenerAdapter() {

    override fun onMessageReceived(event: MessageReceivedEvent) {
        if (!event.isFromGuild || event.message.contentRaw != "!caedump") {
            return
        }
        val guildConfig = stringDatabaseFactory[event.guild]
        try {
            event.channel.sendMessage("Black cards: ${guildConfig.blackCards}").queue()
            event.channel.sendMessage("White cards: ${guildConfig.whiteCards}").queue()
        } catch (e: Exception) {
            event.message.addReaction(EMOJI_ERROR).queue()
            throw e
        }
    }
}
