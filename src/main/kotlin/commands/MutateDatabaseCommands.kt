package com.mukherg.discord.cardsagainsteorzea.commands

import com.mukherg.discord.cardsagainsteorzea.StringDatabaseFactory
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter
import org.slf4j.LoggerFactory

object MutateDatabaseCommands

private val logger = LoggerFactory.getLogger(MutateDatabaseCommand::class.java)!!

abstract class MutateDatabaseCommand : ListenerAdapter() {

    final override fun onMessageReceived(event: MessageReceivedEvent) {
        if (!event.isFromGuild || !event.message.contentRaw.startsWith(prefix)) {
            return
        }

        val textMessage = cleanMessage(event.message.contentDisplay)
        if (!validate(textMessage)) {
            event.message.addReaction(EMOJI_ERROR).queue()
            return
        }

        try {
            mutate(event.guild, textMessage)
            event.message.addReaction(EMOJI_SUCCESS).queue()
        } catch (e: Exception) {
            event.message.addReaction(EMOJI_ERROR).queue()
        }
    }

    abstract val prefix: String

    private fun cleanMessage(message: String): String {
        return message.substring(prefix.length)
    }

    protected abstract fun mutate(guild: Guild, cleanedMessage: String)
    protected abstract fun validate(cleanedMessage: String): Boolean
}

class CardsAgainstEorzeaRemoveBlackCommand(
    private val stringDatabaseFactory: StringDatabaseFactory
) : MutateDatabaseCommand() {
    override val prefix = "!caedelblack "

    override fun mutate(guild: Guild, cleanedMessage: String) {
        stringDatabaseFactory.removeBlackCard(guild, cleanedMessage)
    }

    override fun validate(cleanedMessage: String): Boolean = true
}

class CardsAgainstEorzeaRemoveWhiteCommand(
    private val stringDatabaseFactory: StringDatabaseFactory
) : MutateDatabaseCommand() {
    override val prefix = "!caedelwhite "

    override fun mutate(guild: Guild, cleanedMessage: String) {
        stringDatabaseFactory.removeWhiteCard(guild, cleanedMessage)
    }

    override fun validate(cleanedMessage: String): Boolean = true
}

class CardsAgainstEorzeaAddBlackCommand(
    private val stringDatabaseFactory: StringDatabaseFactory
) : MutateDatabaseCommand() {
    override val prefix = "!caeblack "

    override fun mutate(guild: Guild, cleanedMessage: String) {
        stringDatabaseFactory.addBlackCard(guild, cleanedMessage)
    }

    override fun validate(cleanedMessage: String): Boolean {
        if (!cleanedMessage.contains("{}")) {
            logger.info("Message {} not valid, did not have curlies", cleanedMessage)
            return false
        }
        if (cleanedMessage.length > 1024) {
            return false
        }
        return true
    }
}

class CardsAgainstEorzeaAddWhiteCommand(
    private val stringDatabaseFactory: StringDatabaseFactory
) : MutateDatabaseCommand() {

    override val prefix = "!caewhite "

    override fun mutate(guild: Guild, cleanedMessage: String) {
        stringDatabaseFactory.addWhiteCard(guild, cleanedMessage)
    }

    override fun validate(cleanedMessage: String): Boolean {
        return cleanedMessage.length < 256
    }
}
