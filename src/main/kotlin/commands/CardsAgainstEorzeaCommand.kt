package com.mukherg.discord.cardsagainsteorzea.commands

import com.mukherg.discord.cardsagainsteorzea.CardDeck
import com.mukherg.discord.cardsagainsteorzea.CardDrawer
import com.mukherg.discord.cardsagainsteorzea.Generator
import com.mukherg.discord.cardsagainsteorzea.GuildConfig
import com.mukherg.discord.cardsagainsteorzea.ICardDrawer
import com.mukherg.discord.cardsagainsteorzea.RoomMemberStringDatabase
import com.mukherg.discord.cardsagainsteorzea.StringDatabaseFactory
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter
import org.slf4j.LoggerFactory

private val logger = LoggerFactory.getLogger(CardsAgainstEorzeaCommand::class.java)

class CardsAgainstEorzeaCommand(
    private val generator: Generator,
    private val stringDatabaseFactory: StringDatabaseFactory? = null,
    private val blackCardDeck: CardDeck,
    private val whiteCardDeck: CardDeck
) : ListenerAdapter() {

    override fun onMessageReceived(event: MessageReceivedEvent) {
        if (event.message.contentRaw != "!cae") {
            return
        }
        if (event.isFromGuild) {
            val guildConfig = stringDatabaseFactory?.let { it[event.guild] }
            val members = event.guild.members
            event.channel.sendMessage(
                generator.generate(
                    createBlackCardSupplier(guildConfig),
                    createWhiteCardSupplier(guildConfig, members)
                )
            ).queue()
        }
    }

    private fun createBlackCardSupplier(guildConfig: GuildConfig?): ICardDrawer {
        val blackCardSupplier = listOfNotNull(
            blackCardDeck,
            guildConfig?.let { guildConfig.blackCards }
        )
        return CardDrawer(blackCardSupplier.reduce { l, r -> l + r })
    }

    private fun createWhiteCardSupplier(guildConfig: GuildConfig?, members: List<Member>?): ICardDrawer {
        val whiteCardSuppliers = listOfNotNull(
            whiteCardDeck,
            guildConfig?.let { guildConfig.whiteCards },
            members?.let { RoomMemberStringDatabase(it) }
        )
        return CardDrawer(whiteCardSuppliers.reduce { l, r -> l + r })
    }
}
