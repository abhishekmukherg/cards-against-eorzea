package com.mukherg.discord.cardsagainsteorzea

import org.apache.commons.lang3.StringUtils
import org.slf4j.LoggerFactory

private const val MARKER = "{}"

data class BlackCard(val template: String) {
    fun numWhiteCardsNeeded(): Int = StringUtils.countMatches(template, MARKER)
}

data class WhiteCard(val text: String)

class Generator {
    private val logger = LoggerFactory.getLogger(Generator::class.java)!!
    fun generate(blackCardSupplier: ICardDrawer, whiteCardSupplier: ICardDrawer): String {
        val blackCard = BlackCard(blackCardSupplier.get())
        logger.debug("Got black card: {} needs {} white cards", blackCard, blackCard.numWhiteCardsNeeded())
        val whiteCards = (0 until blackCard.numWhiteCardsNeeded()).map { WhiteCard(whiteCardSupplier.get()) }
        logger.debug("And fetched white cards: {}", whiteCards)
        return whiteCards.fold(blackCard.template) { acc, whiteCard ->
            acc.replaceFirst(MARKER, "<${whiteCard.text}>")
        }
    }
}
