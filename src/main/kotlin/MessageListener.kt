package com.mukherg.discord.cardsagainsteorzea

import java.util.Random
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter
import org.slf4j.LoggerFactory

private val logger = LoggerFactory.getLogger(MessageListener::class.java)

private const val BASE_CARD_WEIGHT = 3
private const val ROOM_MEMBERS_WEIGHT = 1

class MessageListener(
    private val command: String,
    private val generator: Generator,
    private val baseWhiteCardDeck: CardDeck,
    private val baseBlackCardDeck: CardDeck
) : ListenerAdapter() {

    private val random = Random()

    override fun onMessageReceived(event: MessageReceivedEvent) {
        if (event.message.contentRaw != command) {
            return
        }
        val roomMembers = event.guild.let {
            logger.info("Found room members, adding them to the supplier")
            RoomMemberStringDatabase(it.members)
        }
        event.channel.sendMessage(
            generator.generate(
                CardDrawer(baseBlackCardDeck, random),
                CardDrawer(
                    WeightedSublists(baseWhiteCardDeck to BASE_CARD_WEIGHT, roomMembers to ROOM_MEMBERS_WEIGHT),
                    random
                )
            )
        ).queue()
    }
}
