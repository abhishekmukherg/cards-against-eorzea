package com.mukherg.discord.cardsagainsteorzea

import com.google.cloud.datastore.DatastoreOptions
import com.mukherg.discord.cardsagainsteorzea.commands.CardsAgainstEorzeaAddBlackCommand
import com.mukherg.discord.cardsagainsteorzea.commands.CardsAgainstEorzeaAddWhiteCommand
import com.mukherg.discord.cardsagainsteorzea.commands.CardsAgainstEorzeaCommand
import com.mukherg.discord.cardsagainsteorzea.commands.CardsAgainstEorzeaDumpDatabase
import com.mukherg.discord.cardsagainsteorzea.commands.CardsAgainstEorzeaRemoveBlackCommand
import com.mukherg.discord.cardsagainsteorzea.commands.CardsAgainstEorzeaRemoveWhiteCommand
import io.sentry.Sentry
import java.util.Random
import kotlin.system.exitProcess
import net.dv8tion.jda.api.JDABuilder
import org.kohsuke.args4j.CmdLineException
import org.kohsuke.args4j.CmdLineParser
import org.kohsuke.args4j.Option

object Main

private const val whiteCardPath = "com/mukherg/discord/cardsagainsteorzea/white.txt"
private const val blackCardPath = "com/mukherg/discord/cardsagainsteorzea/black.txt"

fun main(args: Array<String>) {
    val arguments = Arguments()
    val parser = CmdLineParser(arguments)
    try {
        parser.parseArgument(*args)
    } catch (e: CmdLineException) {
        System.err.println(e.message)
        System.err.println("cards-against-eorzea [--once]")
        parser.printUsage(System.err)
        System.err.println()
        exitProcess(2)
    }
    if (!arguments.once) {
        Sentry.init()
    }

    val classLoader = Main::class.java.classLoader
    val whiteCards = (classLoader.getResourceAsStream(whiteCardPath)
        ?: throw IllegalStateException("Could not find $whiteCardPath"))
        .bufferedReader(Charsets.UTF_8)
        .use { FortuneLikeStringDatabase(it) }
    val blackCards = (classLoader.getResourceAsStream(blackCardPath)
        ?: throw IllegalStateException("Could not find $blackCardPath"))
        .bufferedReader(Charsets.UTF_8)
        .use { FortuneLikeStringDatabase(it) }
    val generator = Generator()
    if (arguments.once) {
        for (i in 0..arguments.numToPrint) {
            val random = Random()
            println(generator.generate(CardDrawer(blackCards, random), CardDrawer(whiteCards, random)))
        }
        return
    }

    val datastore = DatastoreOptions.getDefaultInstance().service!!
    val factory = CloudStoreStringDatabaseFactory(datastore)

    JDABuilder.createDefault(
        System.getenv("CAE_DISCORD_TOKEN")
            ?: throw IllegalArgumentException("Must supply CAE_DISCORD_TOKEN")
    )
        .apply {
            addEventListeners(
                CardsAgainstEorzeaCommand(generator, factory, blackCardDeck = blackCards, whiteCardDeck = whiteCards),
                CardsAgainstEorzeaAddBlackCommand(factory),
                CardsAgainstEorzeaAddWhiteCommand(factory),
                CardsAgainstEorzeaRemoveBlackCommand(factory),
                CardsAgainstEorzeaRemoveWhiteCommand(factory),
                CardsAgainstEorzeaDumpDatabase(factory)
            )
        }
        .build()
}

class Arguments {
    @Option(name = "--one-shot")
    var once: Boolean = false
        private set

    @Option(name = "--num-to-print")
    var numToPrint: Int = 1
        private set
}
