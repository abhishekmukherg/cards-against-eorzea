package com.mukherg.discord.cardsagainsteorzea

class WeightedSublists<T>(private vararg val lists: Pair<List<T>, Int>) : List<T> {

    override val size = lists.sumBy { it.first.size * it.second }
    private val repeatedSizes: List<Int>
    private val repeatedLists: List<List<T>>

    init {
        val repeatedSizes = mutableListOf<Int>()
        val repeatedLists = mutableListOf<List<T>>()
        var currentSize = 0
        lists.forEach { (l, n) ->
            repeat(n) {
                repeatedSizes.add(currentSize)
                currentSize += l.size
                repeatedLists.add(l)
            }
        }
        this.repeatedSizes = repeatedSizes.toList()
        this.repeatedLists = repeatedLists.toList()
    }

    override fun contains(element: T): Boolean {
        return lists.any { it.first.contains(element) }
    }

    override fun containsAll(elements: Collection<T>): Boolean {
        return lists.any { it.first.containsAll(elements) }
    }

    override fun get(index: Int): T {
        if (index < 0 || index >= size) {
            throw IndexOutOfBoundsException(index)
        }
        val listLocation = repeatedSizes.binarySearch(index).let {
            if (it < 0) {
                -1 * it - 2
            } else {
                it
            }
        }
        val leftSide = repeatedSizes[listLocation]
        return repeatedLists[listLocation][index - leftSide]
    }

    override fun indexOf(element: T): Int {
        (repeatedLists zip repeatedSizes).forEach { (list, leftSide) ->
            val indexOf = list.indexOf(element)
            if (indexOf >= 0) {
                return leftSide + indexOf
            }
        }
        return -1
    }

    override fun isEmpty(): Boolean {
        return lists.isEmpty() || lists.all { it.first.isEmpty() }
    }

    override fun iterator(): Iterator<T> {
        return repeatedLists.map { it.asSequence() }.asSequence().flatten().iterator()
    }

    override fun lastIndexOf(element: T): Int {
        throw NotImplementedError("lastIndexOf not implemented")
    }

    override fun listIterator(): ListIterator<T> {
        throw NotImplementedError("listIterator not implemented")
    }

    override fun listIterator(index: Int): ListIterator<T> {
        throw NotImplementedError("listIterator not implemented")
    }

    override fun subList(fromIndex: Int, toIndex: Int): List<T> {
        throw NotImplementedError("subList not implemented")
    }
}
