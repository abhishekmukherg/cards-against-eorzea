package com.mukherg.discord.cardsagainsteorzea

import com.google.cloud.datastore.Datastore
import com.google.cloud.datastore.Entity
import com.google.cloud.datastore.Key
import com.google.cloud.datastore.StringValue
import com.google.common.cache.CacheBuilder
import com.google.common.cache.CacheLoader
import java.io.BufferedReader
import java.util.Random
import java.util.concurrent.TimeUnit
import net.dv8tion.jda.api.OnlineStatus
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Member

interface ICardDrawer {
    fun get(): String
}

class CardDrawer(
    private val supplier: CardDeck,
    private val random: Random = Random()
) : ICardDrawer {
    private val size = supplier.size

    init {
        if (supplier.isEmpty()) {
            throw IllegalArgumentException("Must supply some suppliers, and they must have some strings")
        }
    }

    override fun get(): String {
        return supplier[random.nextInt(size)]
    }
}

typealias CardDeck = List<String>

class RoomMemberStringDatabase(memberCache: List<Member>) : CardDeck {
    private val strings = memberCache.filter { it.onlineStatus != OnlineStatus.OFFLINE }.map { it.effectiveName }

    override val size = strings.size
    override fun contains(element: String) = strings.contains(element)
    override fun containsAll(elements: Collection<String>) = strings.containsAll(elements)
    override fun get(index: Int) = strings[index]
    override fun indexOf(element: String) = strings.indexOf(element)
    override fun isEmpty() = strings.isEmpty()
    override fun iterator() = strings.iterator()
    override fun lastIndexOf(element: String) = strings.lastIndexOf(element)
    override fun listIterator() = strings.listIterator()
    override fun listIterator(index: Int) = strings.listIterator(index)
    override fun subList(fromIndex: Int, toIndex: Int) = strings.subList(fromIndex, toIndex)
}

class FortuneLikeStringDatabase(reader: BufferedReader) : CardDeck {
    private val strings: List<String>

    init {
        val strings = mutableListOf<String>()
        var currentString: String? = null
        for (readString in reader.lines()) {
            currentString = if (readString == "%") {
                currentString?.let(strings::add)
                null
            } else {
                if (currentString != null) {
                    currentString + readString
                } else {
                    readString
                }
            }
        }
        currentString?.let(strings::add)
        this.strings = strings.toList()
    }

    override val size = strings.size
    override fun contains(element: String) = strings.contains(element)
    override fun containsAll(elements: Collection<String>) = strings.containsAll(elements)
    override fun get(index: Int) = strings[index]
    override fun indexOf(element: String) = strings.indexOf(element)
    override fun isEmpty() = strings.isEmpty()
    override fun iterator() = strings.iterator()
    override fun lastIndexOf(element: String) = strings.lastIndexOf(element)
    override fun listIterator() = strings.listIterator()
    override fun listIterator(index: Int) = strings.listIterator(index)
    override fun subList(fromIndex: Int, toIndex: Int) = strings.subList(fromIndex, toIndex)
}

interface StringDatabaseFactory {
    operator fun get(guild: Guild): GuildConfig
    fun addBlackCard(guild: Guild, card: String): GuildConfig
    fun addWhiteCard(guild: Guild, card: String): GuildConfig
    fun removeBlackCard(guild: Guild, card: String): GuildConfig
    fun removeWhiteCard(guild: Guild, card: String): GuildConfig
}

class CloudStoreStringDatabaseFactory(private val datastore: Datastore) : StringDatabaseFactory {
    companion object {
        private const val KIND: String = "Guild"
        private const val MAX_ENTRIES = 30L
    }

    private val guildCache = CacheBuilder.newBuilder()
        .maximumSize(MAX_ENTRIES)
        .expireAfterWrite(10, TimeUnit.MINUTES)
        .build<String, GuildConfig>(CacheLoader.from { key: String? ->
            val newKey: Key = datastore.newKeyFactory().setKind(KIND).newKey(key)
            val entity: Entity? = datastore.get(newKey)
            if (entity == null) GuildConfig(emptyList(), emptyList()) else GuildConfig.fromEntity(entity)
        }
        )

    override operator fun get(guild: Guild): GuildConfig = guildCache[guild.id]

    override fun addBlackCard(guild: Guild, card: String): GuildConfig {
        val currentConfig = this[guild]
        val newConfig = currentConfig.copy(blackCards = currentConfig.blackCards + card)
        save(guild, newConfig)
        return newConfig
    }

    override fun addWhiteCard(guild: Guild, card: String): GuildConfig {
        val currentConfig = this[guild]
        val newConfig = currentConfig.copy(whiteCards = currentConfig.whiteCards + card)
        save(guild, newConfig)
        return newConfig
    }

    override fun removeBlackCard(guild: Guild, card: String): GuildConfig {
        val currentConfig = this[guild]
        val newConfig = currentConfig.copy(
            blackCards = currentConfig.blackCards.filter { it != card }
        )
        save(guild, newConfig)
        return newConfig
    }

    override fun removeWhiteCard(guild: Guild, card: String): GuildConfig {
        val currentConfig = this[guild]
        val newConfig = currentConfig.copy(
            whiteCards = currentConfig.whiteCards.filter { it != card }
        )
        save(guild, newConfig)
        return newConfig
    }

    private fun save(guild: Guild, config: GuildConfig) {
        val key = datastore.newKeyFactory().setKind(KIND).newKey(guild.id)
        val entity = config.toEntity(key)
        datastore.put(entity)
        guildCache.put(guild.id, config)
    }
}

data class GuildConfig(
    val blackCards: CardDeck,
    val whiteCards: CardDeck
) {
    companion object {
        private const val BLACK_CARDS = "blackCards"
        private const val WHITE_CARDS = "whiteCards"
        fun fromEntity(entity: Entity): GuildConfig {
            val blackCards = entity.getList<StringValue>(BLACK_CARDS).map(StringValue::get)
            val whiteCards = entity.getList<StringValue>(WHITE_CARDS).map(StringValue::get)
            return GuildConfig(blackCards, whiteCards)
        }
    }

    fun toEntity(key: Key): Entity =
        Entity.newBuilder(key)
            .set(BLACK_CARDS, blackCards.map(::StringValue))
            .set(WHITE_CARDS, whiteCards.map(::StringValue))
            .build()
}
