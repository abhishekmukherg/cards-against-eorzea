import com.google.cloud.tools.jib.gradle.JibExtension
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    application
    jacoco
    kotlin("jvm") version "1.3.72"
    id("org.jlleitschuh.gradle.ktlint") version "9.2.1"
    id("com.google.cloud.tools.jib") version "2.2.0"
    id("nebula.release") version "13.0.0"
}

repositories {
    jcenter()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("net.dv8tion:JDA:4.1.1_122")
    implementation("org.apache.commons:commons-lang3:3.7")
    implementation("com.google.cloud:google-cloud-datastore:1.14.0")
    implementation("io.sentry:sentry-logback:1.7.30")
    implementation("ch.qos.logback:logback-classic:1.2.3")
    implementation("args4j:args4j:2.33")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.6.0")
    testImplementation("org.junit.jupiter:junit-jupiter-engine:5.6.0")
    testImplementation("com.natpryce:hamkrest:1.7.0.3")
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

application {
    mainClassName = "com.mukherg.discord.cardsagainsteorzea.MainKt"
}

configure<JibExtension> {
    to.image = "gcr.io/abhishekmukherg/cae"
    container.mainClass = "com.mukherg.discord.cardsagainsteorzea.MainKt"
}

tasks {
    "test"(Test::class) {
        useJUnitPlatform()
    }
    "processResources"(Copy::class) {
        filesMatching("*.properties") {
            expand(mutableMapOf(
                    "version" to project.version
            ))
        }
    }
}

tasks.withType(KotlinCompile::class.java) {
    kotlinOptions {
        this.jvmTarget = "11"
    }
}
